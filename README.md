# **Calibration**

Calibration is a process in which an instrument or piece of equipment’s accuracy is compared with a known and proven standard.
There are different types of calibration that conform to different standards.
